'use strict';

// アプリケーションをコントロールするモジュール
var app = require('app');
// ウィンドウを作成するモジュール
var BrowserWindow = require('browser-window');
// ダイアログ
var dialog = require('dialog');

// クラッシュレポート
require('crash-reporter').start();

// メインウィンドウはGCされないようにグローバル宣言
var mainWindow = null;

// 全てのウィンドウが閉じたら終了
app.on('window-all-closed', function () {
    if (process.platform != 'darwin') {
        app.quit();
    }
});

var ipc = require('ipc');


ipc.on('csv-uploaded', function(event, arg) {
    var fs = require('fs');
    var forge = require('node-forge');
    var csv = require('csv');
    var mail = "";
    var status = "";
    var file = arg[0];
    var salt = arg[1].trim();
    var sub = arg[2]-0;

    // テンポラリファイルを生成
    var tempPath = app.getPath('temp') + "/" + (new Date()).getTime() + ".csv";
    var write_stream = fs.createWriteStream(tempPath);
    
    // CSVパーサを準備
    var parser = csv.parse();
    /*parser.on('readable', function() {
        var i = 0;
        var hash;
        var md;
        var record = [];
        while (record = parser.read()) {
            i++;
            mail = record[0].trim();
            if (record.length >= 2 && mail.length > 4 && mail.indexOf("@") > 0) {
                // YDMP版はステータス行も返す
                status = record[1].trim() - 0;
                if (i % 100 == 0) {
                    event.sender.send('hash-progress', parser.count);  // 送信元へレスポンスを返す
                }
                md = forge.md.sha256.create();
                md.update(salt + mail);
                hash = md.digest().toHex();
                write_stream.write(hash + "," + status + "\n");
            }
            else {
                console.log(mail);
            }
            record = null;
        }
        write_stream.end("\n");
        event.sender.send('hash-complete', tempPath);  // 送信元へレスポンスを返す
    });*/
    var i = 0;
    var md;
    write_stream.on("close",function(){
        event.sender.send('hash-complete', tempPath);
    });
    fs.createReadStream(file).pipe(parser).pipe(csv.transform(function(record){
         i++;
        mail = record[0].trim();
        if (mail.length > 4 && mail.indexOf("@") > 0) {
            if (i % 100 == 0) {
                event.sender.send('hash-progress', [i,parser.count]);  // 送信元へレスポンスを返す
            }
            md = forge.md.sha256.create();
            md.update(salt + mail);
            record[0] = md.digest().toHex();
            if(sub>0&&sub<record.length){
                return record.slice(0,sub);
            }else{
                return record;
            }
        }
        else {
            console.log(mail);
        }
    })).pipe(csv.stringify ())
    .pipe(write_stream);
    
});

// テスト
ipc.on('test-event', function(event, arg) {
    console.log("event fired:"+arg);
    var forge = require('node-forge');
    var util = require('util');
    var fs = require('fs');
    var salt = "95f34b1a1bd69c558b604a81015a097590404e90ebf3d9be35d01601260d66a7";
    var num = arg * 1000;
    // テンポラリファイルを生成
    var tempPath = app.getPath('temp') + "/" + (new Date()).getTime() + ".csv";
    var write_stream = fs.createWriteStream(tempPath);
    var start = (new Date()).getTime();
    var md,mail;
    console.log("memory:"+process.memoryUsage());
    for(var i=0;i<num;i++) {
        mail = "sample"+i+"@example.com";
        if (i % 1000 == 0) {
            console.log("memory["+i+"]:" + ((new Date()).getTime() - start)+"msec:"+util.inspect(process.memoryUsage()));
            //$("#status").html("Processed:" + i + " lines (" + ((new Date()).getTime() - start) + " msec).");
        }
        md = forge.md.sha256.create();
        md.update(salt + mail);
        write_stream.write(md.digest().toHex() + "," + mail + "\n");
    }
    
    md = forge.md.sha256.create();
    md.update(salt + "sample@example.com");
    write_stream.write(md.digest().toHex() + "," + "sample@example.com" + "\n");
    write_stream.end("\n");
    //$("#status").html("Processed:" + ((new Date()).getTime() - start) + " msec.");
    //saveFile(tempPath);
    event.sender.send('test-result', tempPath);  // 送信元へレスポンスを返す
});

// Electronの初期化完了後に実行
app.on('ready', function () {
    // メイン画面の表示。ウィンドウの幅、高さを指定できる
    mainWindow = new BrowserWindow({width: 500, height: 300});
    mainWindow.loadURL('file://' + __dirname + '/index.html');

    // ウィンドウが閉じられたらアプリも終了
    mainWindow.on('closed', function () {
        mainWindow = null;
    });
});