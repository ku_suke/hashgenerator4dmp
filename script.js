var remote = require('remote');
var app = remote.require('app');
var BrowserWindow = remote.require('browser-window');
var dialog = remote.require('dialog');
var fs = remote.require('fs');
var request = remote.require('request');
//var Iconv = remote.require('iconv').Iconv;
//var iconv = new Iconv('SHIFT_JIS', 'UTF-8//TRANSLIT//IGNORE');
window.jQuery = window.$ = require('./jquery-2.1.4.min');

var currentPath = "";

/**
 * ファイルパスを受け取りファイルをロードする
 * @param {type} file
 * @returns {undefined}
 */
var openFile = function(file) {
    
    var salt = $("#salt").val();
    var sub = $("#sub").val();
    window.localStorage.setItem('salt', salt);
    
    var ipc = require("electron").ipcRenderer;
    ipc.on('hash-complete', function(event,arg) {
      console.log(arg); // prints "pong"
      //$("#status").html("Finish!!");
      saveFile(arg);
    });
    ipc.on('hash-progress', function(event,arg) {
        $("#status").html("Processed:" + arg[0]+"/"+arg[1] + " lines");
    });
    
    ipc.send('csv-uploaded', [file,salt,sub]);
    
    
};

var doTest  = function() {
    var num = $("#_num").val();
    var ipc = require("electron").ipcRenderer;
    ipc.on('test-result', function(event,arg) {
      console.log(arg); // prints "pong"
      saveFile(arg);
    });
    ipc.send('test-event', num);
};

var saveFile = function(tempPath) {

    var options = {
        title: 'ハッシュ化CSVを保存',
        defaultPath: app.getPath('userDesktop') + "/hashed.csv",
        filters: [{
            name: 'CSV',
            extensions: ['csv']
        }]
    };
    dialog.showSaveDialog(options, function(filename) {
        var buf;
        if(filename){
            buf = fs.readFileSync(tempPath, "utf-8");
            fs.writeFileSync(filename, buf, 'utf8');
            console.log("saved!!:" + filename);
        }else{
            $("#status").html("Save cancelled (tmp)"+tempPath);
        }
    });
};




var t = null;
document.ondragover = function(e) {
    e.preventDefault();

    if (e.dataTransfer.files[0]) {
        var file = e.dataTransfer.files[0].path;
        if (/\.csv$/.test(file)) {
            window.$("body").css("background-color", "#FFFFCC");
        }
        else {
            window.$("body").css("background-color", "#FFCCCC");
        }
    }
    if (t) {
        clearTimeout(t);
        t = null;
    }
    t = setTimeout(function() {
        $("body").css("background-color", "#FFFFFF");
    }, 500);
    return false;
};

document.ondrop = function(e) {
    e.preventDefault();
    if (e.dataTransfer.files[0]) {
        var file = e.dataTransfer.files[0].path;
        if (/\.csv$/.test(file)) {
            openFile(file);
        }
    }
    if (t) {
        clearTimeout(t);
        t = null;
    }
    $("body").css("background-color", "#FFFFFF");
    return false;
};

var App = {
    showOpenDialog: function() {
        var options = {
            title: 'メールアドレスCSVを選択',
            defaultPath: app.getPath('userDesktop'),
            filters: [{
                name: 'CSV',
                extensions: ['csv']
            }],
            properties: ['openFile']
        };
        dialog.showOpenDialog(options, function(filenames) {
            if (filenames) {
                openFile(filenames[0]);
            }
        });
    }
};